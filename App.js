import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button, Modal } from 'react-native';
import {Alert} from 'react-native';
import * as MailComposer from 'expo-mail-composer';
import * as SMS from 'expo-sms';

/*	R E S O U R S E S
 *	https://reactnavigation.org/docs/headers
 *	https://github.com/expo/expo/issues/7472	(installing expo-mail-composer)
 * 
 *
 * */

const Stack = createStackNavigator();

const BlueScreen = ({navigation}) => {
	return (
		<View style={styles.container}>
		<Text>
		blue
		</Text>
		</View>
	);
}

const GreenScreen = ({navigation, route}) => {
	return (
		<View style={styles.container}>
		<Text>
		green -> 
		{
			route.params.word
		}
		</Text>
		</View>
	);
}

function HomeScreen({navigation}) {
	return (
		<View style={styles.container}>

		<Button title='go to blue screen' onPress={() => 	navigation.navigate('bluescreen')}/>
		<Button title='go to green screen' onPress={() => 	navigation.navigate('greenscreen', {word: 'this is text passed by the navigator'})}/>
		<StatusBar style="auto" />
		</View>
	);
}

// when using navagation consider this function more of an index (like found in the front of a book ) of screens in your app
export default function App() {
	return (
		<NavigationContainer>
		<Stack.Navigator>

		<Stack.Screen
		name="Home"
		component={HomeScreen}
		options={{ title: 'default title' ,
				headerStyle: {
				}}}
		/>
		
		<Stack.Screen 
		name="bluescreen" 
		component={BlueScreen} 
		options={{title: `default screen title `,
				headerStyle: {
				}}}
		/>
		
		<Stack.Screen 
		name="greenscreen" 
		component={GreenScreen} 
		options={{title: `default screen title `,
				headerStyle: {
				}}}
		/>


		</Stack.Navigator>
		</NavigationContainer>
	);
}

const styles = StyleSheet.create({
	container: {
	},
});
